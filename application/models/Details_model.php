<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Details_model extends CI_Model 
{
	public function __construct()
{
    parent::__construct();
    $this->load->database();

}
	
	public function get_details()
	{
		$this->db->select('*');
		$this->db->from('demoproject');
		$query = $this->db->get();
		return $query->result();
	}

}
