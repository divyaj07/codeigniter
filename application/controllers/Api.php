<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('details_model');
		$this->load->helper('url');
		$this->load->helper('text');
    }
    
    public function details()
	{
		header("Access-Control-Allow-Origin: *");

		$details = array();
		
			$texts = $this->details_model->get_details();
			foreach($texts as $text) {
				$details[] = array(
					'title' => $text->title,
					'details' => $text->details
				);
			}

			$this->output
				->set_status_header(200)
				->set_content_type('application/json')
				->set_output(json_encode($details)); 
		
    }
}
